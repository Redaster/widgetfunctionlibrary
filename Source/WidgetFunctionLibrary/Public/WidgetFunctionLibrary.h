// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "WidgetFunctionLibrary.generated.h"

UCLASS()
class UWidgetFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure)
	static UWidget* GetRootWidget(UUserWidget* widget);

	/**
	 * Recursively checks the tree of the widget's parents and polls them for the activity of the child widget. 
	 * @return true if all widgets are active.
	 */
	UFUNCTION(BlueprintPure)
	static bool CheckWidgetIsActive(UWidget* widget);

	/** Gathers descendant child widgets of a parent widget. */
	UFUNCTION(BlueprintCallable, BlueprintCosmetic, Meta = (DefaultToSelf = "widget"))
	static TArray<UWidget*> GetAllWidgetsInTree(UUserWidget* widget);

	/** Returns the content of the slot. */
	UFUNCTION(BlueprintPure, BlueprintCosmetic, Meta = (DefaultToSelf = "widget"))
	static UWidget* GetSlotContent(UPanelSlot* slot);

	/**
	 * Draws a line.
	 *
	 * @param positionA		Starting position of the line in local space.
	 * @param positionB		Ending position of the line in local space.
	 * @param tint			Color to render the line.
	 * @param bAntiAlias	Whether the line should be antialiased.
	 * @param thickness		How many pixels thick this line should be.
	 */
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "4"), Category = "Painting")
	static void DrawLineUnder(UPARAM(ref) FPaintContext& context, FVector2D positionA, FVector2D positionB, FLinearColor tint = FLinearColor::White, bool bAntiAlias = true, float thickness = 1.0f);
};
