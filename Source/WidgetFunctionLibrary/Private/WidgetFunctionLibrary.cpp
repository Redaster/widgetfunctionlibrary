// Copyright Epic Games, Inc. All Rights Reserved.

#include "WidgetFunctionLibrary.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/PanelWidget.h"

UWidget* UWidgetFunctionLibrary::GetRootWidget(UUserWidget* widget)
{
	if (widget)
	{
		return widget->GetRootWidget();
	}
	return nullptr;
}

bool UWidgetFunctionLibrary::CheckWidgetIsActive(UWidget* widget)
{
	if (!IsValid(widget))
	{
		return false;
	}
	
	while (IsValid(widget))
	{
		UWidget* parent = widget->GetParent();
		if (!IsValid(parent) || !parent->GetCachedWidget().IsValid())
		{
			return widget->GetCachedWidget()->GetVisibility() == EVisibility::Visible
				|| widget->GetCachedWidget()->GetVisibility() == EVisibility::HitTestInvisible
				|| widget->GetCachedWidget()->GetVisibility() == EVisibility::SelfHitTestInvisible;
		}
		
		FArrangedChildren arrangedChildren(EVisibility::Visible);
		parent->GetCachedWidget()->ArrangeChildren(parent->GetCachedWidget().Get()->GetPaintSpaceGeometry(), arrangedChildren);
		bool wasFound = false;
		for (int32 i = 0; i < arrangedChildren.Num(); i++)
		{
			if (arrangedChildren[i].GetWidgetPtr() == widget->GetCachedWidget().Get())
			{
				wasFound = true;
				break;
			}
		}
		if (!wasFound)
		{
			return false;
		}
		widget = parent;
	}
	return true;
}

TArray<UWidget*> UWidgetFunctionLibrary::GetAllWidgetsInTree(UUserWidget* widget)
{
	TArray<UWidget*> widgets;
	if (!IsValid(widget) || !IsValid(widget->WidgetTree))
		return widgets;

	widget->WidgetTree->GetAllWidgets(widgets);
	return widgets;
}

UWidget* UWidgetFunctionLibrary::GetSlotContent(UPanelSlot* slot)
{
	return IsValid(slot) ? slot->Content : nullptr;
}

void UWidgetFunctionLibrary::DrawLineUnder(FPaintContext& context, FVector2D positionA, FVector2D positionB, FLinearColor tint, bool bAntiAlias, float thickness)
{
	TArray<FVector2f> points;
	points.Add(UE::Slate::CastToVector2f(positionA));
	points.Add(UE::Slate::CastToVector2f(positionB));

	if ((positionA - positionB).SquaredLength() > KINDA_SMALL_NUMBER)
	{
		FSlateDrawElement::MakeLines(
			context.OutDrawElements,
			context.LayerId / 3,
			context.AllottedGeometry.ToPaintGeometry(),
			points,
			ESlateDrawEffect::None,
			tint,
			bAntiAlias,
			thickness);
	}
}
